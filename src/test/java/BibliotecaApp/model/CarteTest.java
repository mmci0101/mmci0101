package BibliotecaApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {

    private Carte c1,c2,c3;

    @BeforeClass
    public static void setUp() throws Exception {
        System.out.println("Before class Carte");
    }

    @Before
    public void setup()
    {
        c1=new Carte();
       c2=new Carte();
       c2.setTitlu("Ion");
       c2.setAnAparitie("1980");
       //c2.getCarteFromString("Ion;Liviu Rebreanu;2018;Humanitas;mostenire,pamant,Ana");
        System.out.println("setup");
    }

    @After
    public void teardown()
    {
        c1=null;
        c2=null;
    }

    @Test
    public void test_getTitlu()
    {
        assertEquals("egale","Ion",c2.getTitlu());
        assertNotEquals("diferite","Ion",c1.getTitlu());
        System.out.println("getTitlu succes");
    }

    //@Ignore ("testul getTitlu2 e in lucru")
    @Test (expected=NullPointerException.class)
    public void test_getTitlu2()
    {
        System.out.println("a intrat in getTitlu2");
        assertEquals("egale","Ion",c3.getTitlu());
        System.out.println("getTitlu2 succes");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        System.out.println("After class Carte");
    }

////    @Test (timeout = 10)
////    public  void test_cicluInfinit(){
////        int i=0;
////        while (i<i+1)
////            System.out.println(i++);
//    }

    @Test
    public void getAnAparitie()
    {
        assertEquals("egale","1980",c2.getAnAparitie());
        assertNotEquals("diferite", "1980",c1.getAnAparitie());
        System.out.println("getAnAparitie succes");
    }
}