package BibliotecaApp.util;

import BibliotecaApp.model.Carte;
import org.junit.*;
import static org.junit.Assert.*;

public class ValidatorTest {
    Validator v = new Validator();
    Carte c1, c2, c3, c4, c5, c6;
    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Start");
    }

    @Before
    public void setUp() throws Exception {
        c1 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;1000;Unica;cuv,cheie");
        c2 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu1;999;Unica;cuv,cheie");
        c3 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2018;Unica;cuv,cheie");
        c4 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2019;Unica;cuv,cheie");
        c5 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2005;Unica;cuv,cheie");
        c6 = Carte.getCarteFromString("%&^($$;CostacheDumitrescu;2005;Unica;cuv,cheie");
    }
    @Test
    public void isStringOKTest() throws Exception {
        assertTrue("it passes1", v.isStringOK("ygaefuybenwougvbecwi"));
        assertTrue("it passes2", v.isStringOK("OOSDBEBYVBCEDNSJSUDU"));
        assertTrue("it passes3", v.isStringOK("ygaeOOSDBtsdvSDJYVwi"));
        assertTrue("it passes4", v.isStringOK("SGDYysgdYSjskivbecwi"));
        assertTrue("it passes5", v.isStringOK("ygaefuybenwGDhsDHSUJ"));
        assertTrue("it passes6", v.isStringOK("ygaSAsauhHUASkSAecwi"));

        //assertTrue("it CRASHES", v.isStringOK("1324"));
        //assertNotSame("works!", v.isStringOK("1324"));
        //assertFalse("it CRASHES", v.isStringOK("-"));
        //assertTrue("it CRASHES", v.isStringOK("$#%@^&@*#&^@$"));
        System.out.println("PASSED isStringOKTest !");
    }

    @Test
    public void isStringOKTestInvalidString() {
        boolean thrown = false;

        try {
            Validator.isStringOK("----");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("PASSED isStringOKTestInvalidString");
    }


    @Test
    public void validateCarteTest() throws Exception {

    }


    @After
    public void tearDown() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("Finish");
    }
}