package BibliotecaApp.control;

import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import javafx.scene.shape.ClosePathBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BibliotecaCtrlTest {
    Carte c1,c2,c3,c4,c5,c6;
    CartiRepo carterepo;
    @Before
    public void setUp() throws Exception {
        carterepo = new CartiRepo();
        c1 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;1000;Unica;cuv,cheie");
        c2 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu1;999;Unica;cuv,cheie");
        c3 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2018;Unica;cuv,cheie");
        c4 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2019;Unica;cuv,cheie");
        c5 = Carte.getCarteFromString("MareaBrambureala;CostacheDumitrescu;2005;Unica;cuv,cheie");
        c6 = Carte.getCarteFromString("%&^($$;CostacheDumitrescu;2005;Unica;cuv,cheie");
    }

    @After
    public void tearDown() throws Exception {
        carterepo = null;
        c1 = null;
        c2 = null;
        c3 = null;
        c4 = null;
        c5 = null;
        c6 = null;
    }

    @Test
    public void tc1(){
        carterepo.adaugaCarte(c1);
    }

    @Test(expected = Exception.class)
    public void tc2()
            throws Exception {
        carterepo.adaugaCarte(c2);
    }

    @Test(expected = Exception.class)
    public void tc3()
            throws Exception {
        carterepo.adaugaCarte(c2);
    }

    @Test(expected = Exception.class)
    public void tc4()
            throws Exception {
        carterepo.adaugaCarte(c2);
    }
}